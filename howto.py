"""Applies and removes GitLab issue labels on certain cadences."""
import os
import random
from datetime import datetime
from gitlab import Gitlab

# Get GitLab token from environment variable
gitlab_token = os.environ.get('GITLAB_TOKEN')

PROJECT_ID = 47880778

subscriptions = {}
subscriptions["SupportHowTo-EveryWeek"]   = 1
subscriptions["SupportHowTo-EveryTwoWeeks"]   = 2
subscriptions["SupportHowTo-EveryFourWeeks"]  = 4



# Get the current date
current_date = datetime.now()

# Date since 2023-01-01
start_date = datetime(2023, 1, 1)

# Calculate the number of weeks
number_of_weeks = (current_date - start_date).days // 7

print('Running for week #' + str(number_of_weeks) + ' since project start.')

# Initialize GitLab connection
gl = Gitlab('https://gitlab.com', private_token=gitlab_token)

for label, weeks in subscriptions.items():
    if number_of_weeks % weeks == 0:
        print('Picking issue for ' + label)
        # Get project
        project = gl.projects.get(PROJECT_ID)

        # Get closed issues
        closed_issues = project.issues.list(state='closed')

        # Filter deprecated issues
        relevant_closed_issues = [i for i in closed_issues if 'deprecated' not in i.labels]

        # Filter issues that have already been picked
        eligible_issues = [i for i in relevant_closed_issues if label not in i.labels]

        # If there are relevant closed issues, but none are eligable:
        # We already notified about all of them, so we restart the cycle
        if not eligible_issues and relevant_closed_issues:
            print(f'No issues left for "{label}", resetting labels')
            for relevant_closed_issue in relevant_closed_issues:
                relevant_closed_issue.labels.remove(label)
                relevant_closed_issue.save()
                print(f'Label "{label}" removed from issue #{relevant_closed_issue.iid}')
            eligible_issues = relevant_closed_issues

        # Notify about a random issue
        if eligible_issues:
            selected_issue = random.choice(eligible_issues)

            selected_issue.labels.append(label)
            selected_issue.save()

            print(f'Label "{label}" applied to issue #{selected_issue.iid}')
        else:
            print('No eligible issues found.')
    else:
        print('Nothing to do for ' + label)
