# GitLab Support HowTo

![Project Logo](howto.png)

## Table of Contents

- [Project Overview](#project-overview)
- [Usage](#usage)
- [Contributing](#contributing)
- [Future Goals](#future-goals)

## Project Overview

The **GitLab Support HowTo** repository is a collection of practical tutorials, tips, tricks, and tools curated for the GitLab Support team. The "How To" issues serve as a guide on how to perform various tasks, troubleshoot common issues, utilize helpful tools, and more. Each issue is supposed to be "bite-sized", so that it can be read and understood in 5 to 10 minutes.

This makes the repository into a knowledge hub, empowering team members with actionable insights to enhance their workflow efficiency and problem-solving skills – a collection of useful information that does not really have a place in our docs or the handbook, but are helpful enough that they should have a more permanent home than various Slack channels. However, this repository ideally should _not_ become the SSOT for anything. It's supposed to spread _available_ knowledge, in an attempt to help with discoverability – to that end, we always aim to link to a "canonical source" for each item.

If nothing else, this repository is here to enshrine valuable knowledge shared on volatile communication sources, such as Slack, pairing sessions, team meetings, etc. 

## Usage

1. Browse through the list of existing "How To" issues to find tutorials and information relevant to your needs.
   - Open issues are considered "in draft", so you'll want to browse closed issues for finished content. (Of course, [everything is in draft](https://handbook.gitlab.com/handbook/values/#everything-is-in-draft), so closed issues should still be iterated on when it makes sense!)
1. Engage in discussions on existing "How To" issues to suggest improvements, vote them up 👍 (or down 👎), or share your experience using it.
1. Optional: Subscribe to one of the `SupportHowTo-EveryXWeeks` [labels in this project](https://gitlab.com/gitlab-com/support/howto/-/labels?subscribed=&search=SupportHowTo-). A scheduled pipeline applies these labels to a random "How To" at the corresponding intervals, so you will receive a reminder/link in your inbox at the interval of your choice!
   - Notifications are sent out at the beginning of the [Support week](https://about.gitlab.com/support/#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen), Sunday 10PM UTC.

## Contribute!

Everyone on the GitLab Support team is encouraged to contribute to this repository! By creating and curating "How To" issues, you're actively enriching the collective knowledge of the team.

To contribute a "How To":

1. [Create a new issue](https://gitlab.com/gitlab-com/support/howto/-/issues/new?issue%5Btitle%5D=HOWTO&issuable_template=howto_tutorial)
1. Provide a clear and descriptive title for your "How To"
1. Follow the template's guidelines to structure your issue – including step-by-step instructions, code snippets, screenshots, and relevant links as the SSOT.
1. Submit the issue.

You can also contribute by engaging in discussions on existing "How To" issues to suggest improvements, ask clarifying questions, or share additional insights.

Remember, your contributions directly impact the team's growth and effectiveness. Let's learn, share, and excel together!
